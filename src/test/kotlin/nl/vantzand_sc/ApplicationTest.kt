package nl.vantzand_sc

import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.server.testing.*
import nl.vantzand_sc.plugins.configureRouting
import kotlin.test.*

class ApplicationTest {
    @Test
    fun testRoot() = testApplication {
        application {
            configureRouting()
        }
        client.get("http://localhost/google.com/443").apply {
            assertEquals(HttpStatusCode.OK, status)
            assertEquals("{ \"result\": \"SUCCESS\" }", bodyAsText())
        }
    }
}
