package nl.vantzand_sc.plugins

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import nl.vantzand_sc.ConnectionResult
import nl.vantzand_sc.ConnectionType
import nl.vantzand_sc.testConnectivityTo

fun Application.configureRouting() {
    routing {
        get("/{fqdn}/{port}/{type?}") {
            val canConnect = testConnectivityTo(
                call.parameters["fqdn"] ?: "",
                call.parameters["port"]?.toInt() ?: 0,
                ConnectionType.valueOf(call.parameters["type"]?.uppercase() ?: "TCP"),
            )
            val (keyName, status) = when (canConnect) {
                ConnectionResult.SUCCESS -> "result" to HttpStatusCode.OK

                else -> "error" to HttpStatusCode.NotFound
            }
            call.respondText(
                """{
                    |  "$keyName": "${canConnect.name}"
                    |}
                    |""".trimMargin(), ContentType.Application.Json, status
            )
        }
    }
}
