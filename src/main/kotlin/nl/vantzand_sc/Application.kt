package nl.vantzand_sc

import io.ktor.network.selector.*
import io.ktor.network.sockets.*
import io.ktor.server.application.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import nl.vantzand_sc.plugins.configureMonitoring
import nl.vantzand_sc.plugins.configureRouting
import nl.vantzand_sc.plugins.configureSockets
import java.lang.Exception
import java.net.ConnectException
import java.nio.channels.UnresolvedAddressException

fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)
}

fun Application.module() {
    configureSockets()
    configureMonitoring()
    configureRouting()
}

enum class ConnectionResult {
    UNKNOWN,
    ADDRESS_RESOLUTION_FAILED,
    CANNOT_CONNECT,
    SUCCESS
}

enum class ConnectionType {
    TCP,
    UDP
}

fun testConnectivityTo(fqdn: String, port: Int, type: ConnectionType): ConnectionResult {
    return runBlocking {
        try {
            val socketBuilder = aSocket(ActorSelectorManager(Dispatchers.IO))
            withTimeout(2000) {
                val socket = when (type) {
                    ConnectionType.TCP -> socketBuilder
                        .tcp()
                        .connect(
                            fqdn,
                            port
                        )

                    else -> socketBuilder
                        .udp()
                        .connect(InetSocketAddress(fqdn, port))
                }
                socket.close()
            }
            return@runBlocking ConnectionResult.SUCCESS
        } catch (e: UnresolvedAddressException) {
            return@runBlocking ConnectionResult.ADDRESS_RESOLUTION_FAILED
        } catch (e: ConnectException) {
            return@runBlocking ConnectionResult.CANNOT_CONNECT
        } catch (e: TimeoutCancellationException) {
            return@runBlocking ConnectionResult.CANNOT_CONNECT
        } catch (e: Exception) {
            println("${e.javaClass.name}: ${e.message}")
            return@runBlocking ConnectionResult.UNKNOWN
        }
    }
}
