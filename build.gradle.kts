val ktorVersion: String by project
val kotlinVersion: String by project
val logbackVersion: String by project

plugins {
    kotlin("jvm") version "1.9.23"
    id("io.ktor.plugin") version "2.3.10"
    id("com.google.cloud.tools.jib") version "3.4.2"
}

group = "nl.vantzand_sc"
version = "1.0.0-pre-release"

application {
    mainClass.set("io.ktor.server.netty.EngineMain")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.ktor:ktor-server-call-logging-jvm")
    implementation("io.ktor:ktor-server-core-jvm")
    implementation("io.ktor:ktor-server-netty-jvm")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("io.ktor:ktor-server-config-yaml:2.3.10")

    testImplementation("io.ktor:ktor-server-tests-jvm")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlinVersion")
}

jib {
    from {
        image = "openjdk:21-slim"
    }
    to {
        image = "docker-hub.internal.degiro.eu/connectivity-tester"
        tags = mutableSetOf(version.toString(), "latest")
    }
    container {
        jvmFlags = listOf("-Xdebug")
        ports = listOf("8080")
        volumes = listOf("/data")
        environment = mapOf(
            "VERSION" to version.toString(),
            "DATA_DIR" to "/data",
            "APPLICATION_PORT" to "8080",
            "DEVELOPMENT_MODE" to "false"
        )
    }
}

tasks {
    build {
        dependsOn(tasks {
            jib
        })
    }
}
